## Installation

## Building container image

Use **docker** directory for docker and **podman** directory for podman.
The following commands are the same for both docker and podman.

Build container image

```
make build
```

## Usage

Run the application

```
make up
```

Show running containers

```
make ps
```

In order to test locally add necesery lines in /etc/hosts file by executing the following commands:

```
sudo echo "127.0.0.1 sisiya-api.localdomain" >> /etc/hosts
sudo echo "127.0.0.1 sisiya-api-swagger.localdomain" >> /etc/hosts
```

## Initializing git pre hooks

In order to use the predefined git hooks run the following command. It needs
to be run just once.

```
./utils/add_git_hooks.sh
```

# Default admin user

The application creates a default admin user:

```
username: admin
email: admin@example.com
password: admin123654
```

You should change it after the installation.

## Support

Please use gitlab's issue tracker system
https://gitlab.com/a6005/sisiya-api/-/issues if you have any issue or requests.

## Repository

The repository is in GitLab: https://gitlab.com/a6005/sisiya-api/

## Authors and acknowledgment

Fatih GENÇ

Erdal MUTLU

## License

This software is licensed under GPLv3.
