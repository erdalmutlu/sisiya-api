# Prerequisites for local testing

- Add aliases for localhost:

```
sudo echo "127.0.0.1 sisiya-api.localdomain" >> /etc/hosts
sudo echo "127.0.0.1 sisiya-api-swagger.localdomain" >> /etc/hosts
```

# URLs

## Swagger UI

```
http://sisiya-api-swagger.localdomain:18080
```

## API definition file

```
http://sisiya-api.localdomain:8081/api/v1/definition
```

# Initial database configuration

Database initialization is handled by the application itself now.
In case you need to reset it please use the following command.

- Recreate database

```
make reset_db
```

# Building container image and using the application

## Build container image

```
make build
```

## Start containers

```
make up
```

## Stop containers

```
make down
```

## Show containers

```
make ps
```

## Get healtch status

```
make healthcheck
```

## Scan the live image with trivy

```
make scan
```

## Statistics of running containers

```
make stats
```

# Using swagger in production

```
docker run -it --rm --name swagger --publish 8080:8080 -e API_URL=https://sisiya-api.sisiya.de/api/v1/definition docker.io/swaggerapi/swagger-ui
#
podman run -it --rm --name swagger --publish 8080:8080 -e API_URL=https://sisiya-api.sisiya.de/api/v1/definition docker.io/swaggerapi/swagger-ui
```
