#!/bin/sh

set -e

unset BUNDLE_PATH
unset BUNDLE_BIN

cd /usr/src/app

echo "$0: parameters: $*"

# shellcheck disable=SC2145
echo "$0: Executing cmd: $@"
exec "$@"
