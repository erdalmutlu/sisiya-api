#!/usr/bin/env bash
#
## This script removes and creates database volume.
#####################################################################
# shellcheck disable=SC2317
echo "Please use sisiya-backend's reset db script!"
exit 0

cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
  echo "$0: $cmd is not installed on your computer!"
  exit 1
fi

c="sisiya-api-db"
if "$cmd" container inspect "$c" >/dev/null 2>&1 ; then
  echo "Stopping container [$c] ..."
  "$cmd" stop "$c"

  echo "Removing container [$c] ..."
  "$cmd" rm "$c"
fi

v="sisiya_api_db"
if "$cmd" volume inspect "$v" >/dev/null 2>&1; then
  echo "Removing volume [$v] ..."
  "$cmd" volume rm "$v"
fi

echo "Creating volume [$v] ..." 
"$cmd" volume create "$v"

echo "Listing all volumes..."
"$cmd" volume ls

make up

# give it sometime to finish startup
sleep 5

echo "Creating tables and seeding with initial data..."
cd ../app || exit 1
rake db:reset
cd ../"$cmd" || exit 1
