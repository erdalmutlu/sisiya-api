#!/usr/bin/env bash
#
# This script builds podman images.
#
###########################################################################
### defaults
docker_template_file=docker-compose-template.yml
docker_file=docker-compose.yml
env_file=../app/config/initializers/environment.rb
# project_dir=$(pwd)
conf_file=image.conf
image_format=docker
number_of_images=0
project_name=project1
image_names[0]="${project_name}"
image_container_files[0]="app/Dockerfile"
image_versions[0]="__FROM_GIT__"
### end of defaults
###########################################################################
build() {
	build_container_images
}

build_container_images() {
	cd "$project_dir" || exit 1

	# build images in specified formart (docker)
	export BUILDAH_FORMAT=$image_format

	declare -i i=0
	while [ $i -lt $number_of_images ]
	do
		s="${image_names[$i]}"
		v="${image_versions[$i]}"
		c="${image_container_files[$i]}"
		c=${c/__CONTAINER_DIR__/$cmd/}
		if [ "$v" = "__FROM_GIT__" ]; then
			v=$version_str
		fi
		echo "$0: Building image $s:$v..."
		if ! cp -f "$c" . ; then
			echo "$0: Could not copy Container file $c to $project_dir"
			exit 1
		fi

		case "$cmd" in
			docker)
				cd "$cmd" || exit 1
				"$cmd" compose build "$s" || exit 1
				cd "$project_dir" || exit 1
				;;
			podman)	
				echo "$cmd build --file $c --tag ${s}:$v --tag ${s}:latest --label git_commit_tag=$commit_tag --label git_commit_hash=$commit_hash --label git_version=$version_str"
				"$cmd" build . --tag "${s}:$v" --tag "${s}:latest" --label "git_commit_tag=$commit_tag" --label "git_commit_hash=$commit_hash" --label "git_version=$version_str" || exit 1
				;;
		esac
		rm -f Dockerfile
		echo "$0: Saving $s:$v image as $s-${v}.tar ..."
		"$cmd" save -o "$s-${v}.tar" "$s:$v" || exit 1
		mv -f "$s-${v}.tar" "$cmd"/
		i=$((i+1))
	done
}

check_git_porcelain() {
	str=$(git status --porcelain)
	if [[ -n "$str" ]]; then
		echo "$0: The working tree is not clean!"
		git status --porcelain
		echo "$0: Please commit your changes or disgard them and try again"
		exit 1
	fi
}

check_if_already_build() {
	cd "$project_dir/$cmd" || exit 1

	declare -i i=0
	while [ $i -lt $number_of_images ]
	do
		s="${image_names[$i]}"
		v="${image_versions[$i]}"
		if [ "$v" = "__FROM_GIT__" ]; then
			v=$version_str
		fi
		echo "$0: Checking whether image $s:$v is already build or not..."
		if [ ! -f "$s-${v}.tar" ]; then
			return
		else
			echo "$0: $s-${v}.tar already exists. No need to build."
		fi
		i=$((i+1))
	done
	echo "$0: All images are alreading built for commit: $version_str. No need for building." 
	exit 0
}

check_prerequisites() {
	return
}

check_prog() {
	if ! which "$1" ; then
		echo "$0: No such program: $1 !"
		exit 1
	fi 
}

check_progs() {
	for f in bundle curl ; do
		check_prog $f
	done
}

cleanup() {
	rm -rf "$tmp_file" "${project_dir}/Dockerfile"
}

get_branch_name() {
	branch_name=$(git branch --show-current)
}

get_git_info() {
	commit_hash=$(git show --no-patch --format="%H")
	commit_tag=$(git describe --abbrev=0)
	version_str=$(git describe --tags --always)
	if [[ -z "$version_str" ]]; then
		echo "$0: git describe returned epmty string. No tags found!"
		exit 1
	fi
	get_branch_name
}

get_version_from_branch() {
	version_str=$(git branch -v | grep "\*" | cut -d " " -f 2)
}

initialize() {
	check_progs
	set_project_dir
	cd "$project_dir/$cmd" || exit 1
	make create_volume
	if [[ ! -f $conf_file ]]; then
		echo "$0: $conf_file file does not exist!"
		exit 1
	fi
	# source the conf
	# shellcheck disable=SC1090
	. $conf_file

	get_git_info
	# update_app_secrets
	update_version
	check_prerequisites
	check_git_porcelain
	check_if_already_build
	prepare_api
}

prepare_api() {
	cd "$project_dir/app" || exit 1

	rm -f spec/support/api/schemas/*.json
	rm -f logs/*.log

	bundler_version=$(grep -A1 "BUNDLED WITH" Gemfile.lock | grep -v BUNDLED | tr -d " ")
	if ! bundle "_${bundler_version}_" version >/dev/null 2>&1 ; then
		echo "$0: Installing bundler version $bundler_version"
		gem install bundler -v "$bundler_version" || exit 1
	fi

	echo "$0: Installing gems ..."
	# Gemfile.lock must come from git repo. 
	# rm -f Gemfile.lock
	echo "$0: bundle $bundler_version install"

	bundle "_${bundler_version}_" install
}

set_build_env() {
	case "$branch_name" in
		test)
			build_env="test"
			;;
		master | main)
			build_env="prod"
			;;
		*)
			build_env="dev"
			;;
	esac
	echo "$0: Build environment is $build_env"
}

set_project_dir() {
	pwd_str=$(pwd)
	str=$(basename "$pwd_str")
	# check whether we are invoked within Jenkins or not
	if [[ -z ${JENKINS_HOME+x} ]]; then
		# not within Jenkins
		project_dir=$(git rev-parse --show-toplevel)
	else
		# within Jankins
		if [[ -z ${WORKSPACE+x} ]]; then
			project_dir="${pwd_str}"
		else
			project_dir="${WORKSPACE}"
		fi
	fi
}

update_app_secrets() {
	rm -f "$tmp_file"
	while IFS= read -r line
	do
		str=$(echo "$line" | grep "^APPSECRET = " | grep -v "ENV")
		if [ -n "$str" ]; then
			s=$(LC_ALL=C tr -dc 'A-Za-z0-9' </dev/urandom | head -c 40)
			line="APPSECRET = \"$s\""
		fi
		echo "$line" >> "$tmp_file"
	done < $env_file
	cp "$env_file" "${env_file}.org"
	mv "$tmp_file" "$env_file"
	mv "${env_file}.org" "$env_file"
}

update_version() {
	v="Version: $version_str"
	echo "$v" > ../app/VERSION
	{
		echo "# frozen_string_literal: true"
		echo ""
		echo "VERSION = '$version_str'" 
	} > ../app/config/initializers/version.rb
	set_build_env
	if [[ "$cmd" == "docker" ]]; then
		sed -e "s/__VERSION__/$version_str/" \
		  -e "s/__BUILD_ENV__/$build_env/g" \
		  -e "s/__COMMIT_HASH__/$commit_hash/" \
		  -e "s/__COMMIT_TAG__/$commit_tag/" \
		  "$docker_template_file" > "$docker_file"
	fi
}
###########################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

tmp_file=$(mktemp)
trap cleanup EXIT

commit_hash=""
version_str=""

initialize
build
