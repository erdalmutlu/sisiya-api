#!/usr/bin/env bash
#
# This script runs rspec tests.
#
#####################################################################
cd ../app || exit 1
rake db:reset
rake api:generate_json_schemas
sleep 5
rspec
