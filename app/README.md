# Install bundler

- Get bundler version used for this application

```
v=$(grep -A1 "BUNDLED WITH" Gemfile.lock | grep -v BUNDLED | tr -d " ")
```

- Install bundler

```
if ! bundle _${v}_ version >/dev/null ; then gem install bundler -v "$v" ; fi
```

- Install gem files

```
bundle _"$v"_ install
```

# Update Gemfile.lock

- When you change **Gemfile**, then you need to update the **Gemfile.lock** as well

```
bundle _"$v"_ update
```

# Using the console

You can use the **console** to get various infos. Because the environment needs
some variables set, please use the console within the container.

## Start the **console**

```
docker exec -it sisiya-api ./console
podman exec -it sisiya-api ./console
```

The following are some examples.

## Settings
