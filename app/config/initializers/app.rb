# frozen_string_literal: true

API_URL = Settings.api_url
API_PATH_PREFIX = Settings.api_path_prefix
