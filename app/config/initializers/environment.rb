# frozen_string_literal: true

require 'bundler/setup'

ENVIRONMENT = ENV['ENVIRONMENT'] || (ENV['RACK_ENV'] || 'development')
ENV['RACK_ENV'] = ENV['ENVIRONMENT'] = ENVIRONMENT.to_s
Bundler.require(:db, ENVIRONMENT.to_sym)
Bundler.require(:default, ENVIRONMENT.to_sym)
Config.load_and_set_settings(Config.setting_files(CONFIG_PATH, ENVIRONMENT))
