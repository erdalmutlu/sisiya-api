# frozen_string_literal: true

if Object.const_defined?('DB')
  AUTH_DB = DB unless Object.const_defined?('AUTH_DB')

  Sequel::Model.plugin :singular_table_names
  begin
    SequelAccount.setup do |config|
      config.min_password_length = Settings.min_password_length
      # we are not using JWT with the SequelAccount any more
      # config.jwt_secret = Settings.jwt_secret
      # config.jwt_algorithm = Settings.jwt_algorithm
      config.db = AUTH_DB
    end
  rescue StandardError
    APP_LOGGER.fatal 'config/initializers/sequel_account.rb: Unknown DB issue!'
  end
end
