# frozen_string_literal: true

HTTP = Rack::Utils::SYMBOL_TO_STATUS_CODE
http_desc_temp = {}
HTTP.each do |k, v|
  http_desc_temp[k] = Rack::Utils::HTTP_STATUS_CODES[v]
end
HTTP_DESC = http_desc_temp.freeze
