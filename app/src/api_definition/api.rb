# frozen_string_literal: true

require API_DEFINITION_PATH.join('api_definition_helper.rb')
require INITIALIZERS_PATH.join('http_status_codes.rb')
require 'ostruct'

API_SECURITY = OpenStruct.new(type: 'http', scheme: 'bearer', bearerFormat: 'JWT', name: 'BearerAuth')
API = Apigen::Rest::Api.new
API.title Settings.api.title
API.description Settings.api.description
API.version Settings.api.version

API.contact do
  name Settings.api.contact.name
  email Settings.api.contact.email
  url Settings.api.contact.url
end

API.server "#{API_URL}/#{API_PATH_PREFIX}"

API.security_scheme API_SECURITY.type, API_SECURITY.name do
  scheme API_SECURITY.scheme
end

API.model :message do
  description 'Message Response Object'
  type :object do
    message(:string).explain do
      description 'The message'
      example 'Some message'
    end
  end
end

API.model :result do
  description 'Response Object'
  type :object do
    message(:string).explain do
      description 'The message'
      example 'Some message'
    end
  end
end

%w[
  healthcheck auth services systems users
].each do |e|
  require_relative "endpoints/#{e}"
  # require_relative "models/#{e}" if File.exist?("models/#{e}.rb")
  require_relative "models/#{e}"
end
