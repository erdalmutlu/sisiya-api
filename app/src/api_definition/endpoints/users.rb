# frozen_string_literal: true

security_type = API_SECURITY.name

endpoint_tag = 'Users'
endpoint_path = '/users'
endpoint_id_path = '/users/{id}'

API.tag endpoint_tag do
  description 'Users endpoint'
end

API.endpoint :create_user do
  description 'Create a user'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    # type :user_object
    type :create_user_request_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :created
        type :user_response_object
      else
        type :message
      end
    end
  end
end

ApiHelper.create_request_model(API, 'users', 'CreateUser')

API.endpoint :get_user do
  description 'Get a user'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :user_response_object
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'users', 'GetUser')

API.endpoint :get_users do # rubocop:disable Metrics/BlockLength
  description 'Get users'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    email(:string?).explain do
      description 'Email (optional)'
      example 'user1@example.com'
    end

    username(:string?).explain do
      description 'Username (optional)'
      example 'user1'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :user_response_object_array
      else
        type :message
      end
    end
  end
end
