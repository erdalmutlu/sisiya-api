# frozen_string_literal: true

security_type = '[]'
endpoint_tag = 'Authentications'
endpoint_path = '/auth/token'

API.tag endpoint_tag do
  description 'Create tokens endpoint'
end

API.endpoint :create_token do
  description 'Create access and refresh tokens'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :create_token_request_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :created
        type :create_token_response_object
      else
        type :message
      end
    end
  end
end

ApiHelper.create_request_model(API, 'auth', 'CreateToken')

security_type = API_SECURITY.name
endpoint_tag = 'Authentications'
endpoint_path = '/auth/refresh'

API.tag endpoint_tag do
  description 'Create tokens using refresh token endpoint'
end

API.endpoint :refresh_token do
  description 'Refresh tokens'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :created
        type :create_token_response_object
      else
        type :message
      end
    end
  end
end
