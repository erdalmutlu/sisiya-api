# frozen_string_literal: true

security_type = API_SECURITY.name

endpoint_tag = 'Services'
endpoint_path = '/services'
endpoint_id_path = '/services/{id}'

API.tag endpoint_tag do
  description 'Services endpoint'
end

API.endpoint :get_service do
  description 'Get service'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request no_content not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :services_response_object
      else
        type :message
      end
    end
  end
end

API.endpoint :get_services do
  description 'Get services'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    name(:string?).explain do
      description 'service name (optional)'
      example 'filesystem'
    end
  end

  %i[ok bad_request no_content unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :services_response_object_array
      else
        type :message
      end
    end
  end
end
