# frozen_string_literal: true

security_type = API_SECURITY.name

endpoint_tag = 'Systems'
endpoint_path = '/systems'
endpoint_id_path = '/systems/{id}'

API.tag endpoint_tag do
  description 'Systems endpoint'
end

API.endpoint :get_system do
  description 'Get a system'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request no_content not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :systems_response_object
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'systems', 'GetSystem')

API.endpoint :get_systems do # rubocop:disable Metrics/BlockLength
  description 'Get systems'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    name(:string?).explain do
      description 'system name (optional)'
      example 'system1.example.com'
    end

    ip(:string?).explain do
      description 'IP (optional)'
      example '192.168.1.1'
    end
  end

  %i[ok bad_request no_content unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :systems_response_object_array
      else
        type :message
      end
    end
  end
end

endpoint_path = '/systems/services'

API.tag endpoint_tag do
  description 'System services endpoint'
end

API.endpoint :get_systems_services do
  description 'Get a system services'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    name(:string?).explain do
      description 'system name (optional)'
      example 'system1.example.com'
    end
  end

  %i[ok bad_request no_content unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :systems_services_response_object_array
      else
        type :message
      end
    end
  end
end
