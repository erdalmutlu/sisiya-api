# frozen_string_literal: true

API.model :user_object do
  description 'User Object'
  type :object do
    email(:string).explain do
      description 'Email'
      example 'admin@example.com'
    end

    username(:string).explain do
      description 'Username'
      example 'admin'
    end
  end
end

# API.model :create_user_request_object do
#   description 'Create User Request Object'
#   type :object do
#     name(:string).explain do
#       description 'User name'
#       example 'user1.example.com'
#     end
#
#     ip(:string).explain do
#       description 'User IP'
#       example '192.168.1.1'
#     end
#   end
# end

API.model :user_response_object do
  description 'User response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    email(:string).explain do
      description 'Email'
      example 'admin@example.com'
    end

    username(:string).explain do
      description 'Username'
      example 'admin'
    end

    account_uuid(:int32).explain do
      description 'Account user ID'
      example '1'
    end
  end
end

API.model :user_response_object_array do
  description 'User array response object'
  type :array do
    type :user_response_object
  end
end
