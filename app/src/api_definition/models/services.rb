# frozen_string_literal: true

API.model :services_response_object do # rubocop:disable Metrics/BlockLength
  description 'Services response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    system_id(:int32).explain do
      description 'System ID'
      example '1'
    end

    name(:string).explain do
      description 'Services name'
      example 'filesystem'
    end

    expire(:string).explain do
      description 'Expire in minutes'
      example '15'
    end

    created_at(:string).explain do
      description 'Created at'
      example '2024-07-01 10:11:16 +0200'
    end

    updated_at(:string).explain do
      description 'Updated at'
      example '2024-08-21 22:11:16 +0200'
    end
  end
end

API.model :services_response_object_array do
  description 'Services array response object'
  type :array do
    type :services_response_object
  end
end
