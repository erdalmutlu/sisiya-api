# frozen_string_literal: true

API.model :system_object do
  description 'System Object'
  type :object do
    name(:string).explain do
      description 'System name'
      example 'system1.example.com'
    end

    ip(:string).explain do
      description 'System IP'
      example '192.168.1.1'
    end
  end
end
API.model :systems_response_object do # rubocop:disable Metrics/BlockLength
  description 'System response object'
  type :object do # rubocop:disable Metrics/BlockLength
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    name(:string).explain do
      description 'System name'
      example 'system1.example.com'
    end

    active(:bool).explain do
      description 'Is system active'
      example 'true|false'
    end

    effects_global(:bool).explain do
      description 'Does this system effects the overall status or not'
      example 'true|false'
    end

    fqdn(:string).explain do
      description 'Fully qualified domain name'
      example 'system1.example.com'
    end

    ip(:string).explain do
      description 'System IP'
      example '192.168.1.1'
    end

    mac(:string).explain do
      description 'MAC address'
      example 'aa:bb:cc:dd:ee'
    end

    created_at(:string).explain do
      description 'Created at'
      example '2024-07-01 10:11:16 +0200'
    end

    updated_at(:string).explain do
      description 'Updated at'
      example '2024-08-21 22:11:16 +0200'
    end

    status_image(:string).explain do
      description 'Status Image name'
      example 'Error.png | Warning.png | Ok.png | Info.png | Noreport.png | NoreportGreen.png | Unavailable.png'
    end

    status_description(:string).explain do
      description 'Status description'
      example 'file_system(error)'
    end
  end
end

API.model :systems_response_object_array do
  description 'System array response object'
  type :array do
    type :systems_response_object
  end
end

API.model :systems_services_object do
  description 'System Service Object'
  type :object do
    name(:string).explain do
      description 'System name'
      example 'system1.example.com'
    end

    ip(:string).explain do
      description 'System IP'
      example '192.168.1.1'
    end
  end
end

API.model :systems_services_response_object do # rubocop:disable Metrics/BlockLength
  description 'System services response object'
  type :object do # rubocop:disable Metrics/BlockLength
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    system_id(:int32).explain do
      description 'system id'
      example '1'
    end

    service_id(:int32).explain do
      description 'service id'
      example '1'
    end

    status_id(:int32).explain do
      description 'status id'
      example '1'
    end

    description(:string).explain do
      description 'Description'
      example 'The system is OK'
    end

    expire(:int32).explain do
      description 'Expires in minutes'
      example '15'
    end

    created_at(:string).explain do
      description 'Created at'
      example '2024-07-01 10:11:16 +0200'
    end

    changed_at(:string).explain do
      description 'Changed at'
      example '2024-07-01 10:11:16 +0200'
    end

    updated_at(:string).explain do
      description 'Updated at'
      example '2024-08-21 22:11:16 +0200'
    end

    status_name(:string).explain do
      description 'Status name'
      example 'error | warning | ok | noreport'
    end

    service_name(:string).explain do
      description 'Service name'
      example 'system'
    end

    system_name(:string).explain do
      description 'System name'
      example 'client1.example.com'
    end
  end
end

API.model :systems_services_response_object_array do
  description 'System array response object'
  type :array do
    type :systems_services_response_object
  end
end
