# frozen_string_literal: true

API.model :create_token_response_object do
  description 'Create Token Response Object'
  type :object do
    access_token(:string).explain do
      description 'Access token'
      example '<header>.<payload>.<signature>'
    end
    refresh_token(:string).explain do
      description 'Refresh token'
      example '<header>.<payload>.<signature>'
    end
  end
end
