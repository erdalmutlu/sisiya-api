# frozen_string_literal: true

require_relative '../../environment'
Bundler.require(:test)
require 'simplecov'
require 'simplecov_json_formatter'
require 'net/http'
require 'net/https'

# Generate HTML and JSON reports
SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
                                                                  SimpleCov::Formatter::HTMLFormatter,
                                                                  SimpleCov::Formatter::JSONFormatter
                                                                ])

SimpleCov.minimum_coverage Settings.testing.min_coverage.to_i
SimpleCov.start
SimpleCov.at_exit do
  op = SimpleCov.result.coverage_statistics[:line].percent
  color = case op
          when 0..79 then :red
          when 80..94 then :yellow
          else
            :green
          end
  File.write('coverage.svg', Net::HTTP.get(URI.parse("https://img.shields.io/badge/coverage-#{op.round(2)}-#{color}.svg")))
  SimpleCov.result.format!
end

Dir[INITITIALIZERS_PATH.join('init.rb')].sort.each { |file| require file }
require INTERACTIONS_PATH.join('init')
require HELPERS_PATH.join('active_interaction_runner.rb')
ENV['ENVIRONMENT'] = 'test'

RSpec.configure do |config|
  config.add_setting :api_key
  config.add_setting :mock_ip
  config.add_setting :valid_account_params

  config.before :suite do
    RSpec.configuration.api_key = 'LVTCsaDuZZfCBPrOzTNYU4iKTr7bUmvj'
    RSpec.configuration.valid_account_params = []
    RSpec.configuration.mock_ip = '127.0.0.1'

    5.times do |i|
      RSpec.configuration.valid_account_params << { email: "test#{i}@testdomain.com", password: 'test123654',
                                                    password_confirmation: 'test123654' }
    end
  end
end
