# frozen_string_literal: true

require 'roda'
# require HELPERS_PATH.join('active_interaction_runner.rb')

require API_DEFINITION_PATH.join('api')
require INTERACTIONS_PATH.join('init')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')

# Main class of the application
class App < Roda # rubocop:disable Metrics/ClassLength
  include ::Helpers::Application

  opts[:check_dynamic_arity] = false
  opts[:check_arity] = :warn
  opts[:root] = APP_ROOT

  plugin :all_verbs
  use Rack::Cors do
    allowed_methods = %i[get post put delete options head]
    allow do
      origins '*'
      resource '*', headers: :any, methods: allowed_methods
    end
  end

  plugin :default_headers,
         'Content-Type' => 'application/json',
         # 'Strict-Transport-Security'=>'max-age=16070400;', # Uncomment if only allowing https:// access
         'X-Frame-Options' => 'deny',
         'X-Content-Type-Options' => 'nosniff',
         'X-XSS-Protection' => '1; mode=block'

  plugin :content_security_policy do |csp|
    csp.default_src :none
    csp.style_src :self
    csp.form_action :self
    csp.script_src :self
    csp.connect_src :self
    csp.base_uri :none
    csp.frame_ancestors :none
  end

  plugin :common_logger, $stdout

  if ENVIRONMENT == 'development'
    plugin :exception_page

    # RodaRequest
    class RodaRequest
      def assets
        exception_page_assets
        super
      end
    end
  end

  # I18n
  I18n::Backend::Simple.include I18n::Backend::Fallbacks
  I18n.config.available_locales = %i[en tr]
  I18n.load_path = Dir[File.join(SRC_PATH, 'i18n', '*.yml')]
  I18n.backend.load_translations

  plugin :halt
  plugin :hash_routes

  require_relative 'routes/init'

  route do |r|
    r.get ['', true] do
      'The app is running.'
    end
    r.hash_routes
  end

  def init_app
    init_db
    return unless db_ok?

    # APP_LOGGER.debug 'app.rb:init_app: before migration'
    # APP_LOGGER.debug DB.tables

    migrate_db unless db_migrated?

    # APP_LOGGER.debug 'app.rb:init_app: after migration'
    # APP_LOGGER.debug DB.tables
    create_admin_account unless admin_account_ok?
  end

  def admin_account_ok?
    return false unless db_ok?

    APP_LOGGER.debug 'app.rb:admin_ok?: Checking admin account ...'
    return true if User.find(username: 'admin')

    false
  rescue NameError
    APP_LOGGER.debug 'app.rb:admin_ok?: Failed User.find!'
    # activate singular table names, this is needed when db connection
    # fails and is available afterwards
    Sequel::Model.plugin :singular_table_names
    load INITIALIZERS_PATH.join('sequel_account.rb')
    load MODELS_PATH.join('user.rb')
    false
  end

  def create_admin_account
    return true if admin_account_ok?

    APP_LOGGER.debug 'app.rb:create_admin_account: Creating admin account...'
    User.create(username: 'admin', email: 'admin@example.com', password: 'admin123654',
                password_confirmation: 'admin123654')
  end

  def healthy?
    init_app

    is_ok = true
    @app_health_info = { 'DB connection': 'OK' }

    unless db_ok?
      is_ok = false
      @app_health_info[:'DB connection'] = 'FAILED'
    end

    is_ok
  end

  def db_migrated?
    APP_LOGGER.debug "app.rb:db_migrated?: Checking db migrated file: #{DB_MIGRATED_FILE}"
    File.file?(DB_MIGRATED_FILE)
  end

  def migrate_db
    return false unless db_ok?

    APP_LOGGER.debug "app.rb:migrate_db: migrating db and creating migrated file: #{DB_MIGRATED_FILE}"
    return false if system('rake db:migrate') && !File.write(DB_MIGRATED_FILE, '')

    APP_LOGGER.debug "app.rb:migrate_db: migrated db and created db migrated file: #{DB_MIGRATED_FILE}"
    true
  end

  def db_ok?
    return false unless Object.const_defined?('DB')

    APP_LOGGER.debug 'app.rb:db_ok?: checking the db connection...'
    DB.valid_connection?(DB)
  end

  def init_db
    return true if db_ok?

    APP_LOGGER.debug 'app.rb:init_db: Initializing DB connection...'
    begin
      APP_LOGGER.error 'app.rb:init_db: DB connection is down! Trying to connect...'
      load INITIALIZERS_PATH.join('db.rb')
      # we cannot get the return code of the above load, that is why we check here
      db_ok?
    rescue Sequel::DatabaseConnectionError, Sequel::DatabaseDisconnectError
      APP_LOGGER.fatal 'app.rb:init_db_connection: DB connection failed! Giving up...'
      false
    end
  end

  def run_interaction(interaction, options = {})
    init_app

    return api_response http: :internal_server_error, message: 'No DB connection!' unless db_ok?

    Interactions::Runner.run(interaction, request, options)
  end
end
