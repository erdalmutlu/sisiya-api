# frozen_string_literal: true

# Class for system model
class SystemService < Sequel::Model
  many_to_one :service
  many_to_one :status
  many_to_one :system

  def set_status
    total_status = 0
    Service.where(system_id: id).distinct(:status).each do |s|
      total_status += s.status
    end

    update(status: total_status) unless status == total_status
  end

  def self.create_system(name)
    system = System.find(name: name)
    return system if system

    System.create(name: name)
  end
end
