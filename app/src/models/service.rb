# frozen_string_literal: true

require HELPERS_PATH.join('sisiya_service_status')

# Class for service model
class Service < Sequel::Model
  one_to_many :service_data, class: :ServiceData
  one_to_many :latest_service_data, class: :ServiceDataLatest
  many_to_one :system
  extend SisiyaServiceStatus::ClassMethods

  def set_status # rubocop:disable Metrics/AbcSize
    total_status = 0
    ServiceDataLatest.where(service_id: id).distinct(:status).each do |x|
      total_status += if Time.now - x.update_date > expire * 60
                        Service.status_values[:noreport]
                      else
                        Service.status_values[x.status.to_sym]
                      end
    end
    update(status: total_status) if status != total_status
  end

  def self.create_service(system, name, expire)
    service = Service.find(system_id: system.id, name: name)
    if service && service.expire != expire
      service.update(expire: expire)
      return service
    end
    return service if service

    Service.create(system_id: system.id, name: name, expire: expire)
  end
end
