# frozen_string_literal: true

# require HELPERS_PATH.join('sisiya_service_status')

# Class for system model
class System < Sequel::Model
  one_to_many :services

  # include ::SisiyaServiceStatus

  def set_status
    total_status = 0
    Service.where(system_id: id).distinct(:status).each do |s|
      total_status += s.status
    end

    update(status: total_status) unless status == total_status
  end

  def self.create_system(name)
    system = System.find(name: name)
    return system if system

    System.create(name: name)
  end

  def status_description # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    ds = SystemService.dataset.join(:status, id: :status_id)
    ds = ds.join(:service, id: Sequel[:system_service][:service_id])
    ds = ds.where(system_id: id)
    ds = ds.where { Sequel[:status_id] >= 4 }
    ds = ds.select_all(:system_service)
    ds = ds.select_append(Sequel.as(Sequel[:status][:name], :status_name))
    ds = ds.select_append(Sequel.as(Sequel[:service][:name], :service_name))
    ds = ds.distinct(:status_id)
    ds = ds.order(Sequel.desc(:status_id))
    d = ''
    ds.each do |s|
      d += "#{s[:service_name]}(#{s[:status_name]})"
    end
    return 'System is OK' if d == ''

    d
  end

  def status_image
    total_status = 0
    SystemService.where(system_id: id).distinct(:status_id).all.each do |s|
      total_status += s.status_id
    end
    status_image_name(total_status)
  end

  # t = total of uniq statusid's
  #
  #  info        : t  =  1          => I on blue                         => Info.png
  #  ok          : 1  <  t < 4      => Check on green                    => Ok.png
  #  warning     : 4  <= t < 8      => ! on yellow                       => Warning.png
  #  error       : 8  <= t < 16     => ! on red                          => Error.png
  #  noreport    : 16 <= t < 32     => ? on plain color                  => Noreport.png
  #              : (t-16) < 4       => ? on green                        => NoreportGreen.png
  #              : (t-16) < 8       => ? on yellow                       => NoreportYellow.png
  #              : (t-16) < 16      => ? on error                        => NoreportRed.png
  #  unavailable : 32 <= t < 64     => x on plain color                  => Unavailable.png
  #              : (t-32) < 4       => x on green                        => UnavailableGreen.png
  #              : (t-32) < 8       => x on yellow                       => UnavailableYellow.png
  #              : (t-32) < 16      => x on error                        => UnavailableRed.png
  #  mwarning    : 64 <= t < 128    => ! on yellow with maintanance sign => MWarning.png
  #              : (t-64) < 4       => mwarning on green                 => MWarningGreen.png
  #              : (t-64) < 8       => mwarning on yellow                => MWarning.png
  #              : (t-64) < 16      => mwarning on red                   => MWarningRed.png
  #  merror      : 128 <= t < 256   => ! on red with maintanance sign    => MError.png
  #              : (t-128) < 4      => merror on green                   => MErrorGreen.png
  #              : (t-128) < 8      => merror on yellow                  => MErrorYellow.png
  #              : (t-128) < 16     => merror on red                     => MError.png
  #  mnoreport   : 256 <= t < 512   => ? on red with maintanance sign    => MNoreport.png
  #              : (t-256) < 4      => mnoreport on green                => MNoreportGreen.png
  #              : (t-256) < 8      => mnoreport on yellow               => MNoreportYellow.png
  #              : (t-256) < 16     => mnoreport on red                  => MNoreportError.png
  #  munavailable: 512 <= t < 1024  => x on red with maintanance sign    => MUnavailable.png
  #              : (t-512) < 4      => munavailable on green             => MUnavailableGreen.png
  #              : (t-512) < 8      => munavailable on yellow            => MUnavailableYellow.png
  #              : (t-512) < 16     => munavailable on red               => MUnavailableRed.png

  def status_image_name(total_status) # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
    return 'Info.png' if total_status < SISIYA_STATUS[:ok]

    # return 'Ok.png' if total_status > SISIYA_STATUS[:info] && total_status < SISIYA_STATUS[:warning]
    return 'Ok.png' if total_status < SISIYA_STATUS[:warning]

    # return 'Warning.png' if total_status >= SISIYA_STATUS[:warning] && total_status < SISIYA_STATUS[:error]
    return 'Warning.png' if total_status < SISIYA_STATUS[:error]

    # return 'Error.png' if total_status >= SISIYA_STATUS[:error] && total_status < SISIYA_STATUS[:noreport]
    return 'Error.png' if total_status < SISIYA_STATUS[:noreport]

    # return 'Noreport.png' if total_status >= SISIYA_STATUS[:noreport] && total_status < SISIYA_STATUS[:unavailable]
    # return 'Noreport.png' if total_status == SISIYA_STATUS[:noreport]

    return sub_image_name(total_status, :noreport) if total_status < SISIYA_STATUS[:unavailable]

    return sub_image_name(total_status, :unavailable) if total_status < SISIYA_STATUS[:mwarning]

    return sub_image_name(total_status, :mwarning) if total_status < SISIYA_STATUS[:merror]

    return sub_image_name(total_status, :merror) if total_status < SISIYA_STATUS[:mnoreport]

    return sub_image_name(t, :mnoreport) if total_status < SISIYA_STATUS[:munavailable]

    sub_image_name(total_status, :munavailable)
  end

  def sub_image_name(total_status, derived_status)
    image_prefix = SISIYA_STATUS_NAME[derived_status]
    return "#{image_prefix}.png" if total_status == SISIYA_STATUS[derived_status]

    t = total_status - SISIYA_STATUS[derived_status]
    return "#{image_prefix}Green.png" if t < SISIYA_STATUS[:warning]

    return "#{image_prefix}Yellow.png" if t < SISIYA_STATUS[:error]

    "#{image_prefix}Red.png" # if t < SISIYA_STATUS[:noreport]
  end
end
