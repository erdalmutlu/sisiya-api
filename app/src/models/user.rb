# frozen_string_literal: true

# Class for user model
class User < Sequel::Model
  plugin :sequel_account

  def before_create
    create_account
  end
end
