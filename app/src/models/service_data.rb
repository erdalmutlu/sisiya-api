# frozen_string_literal: true

# Class for service_data model
class ServiceData < Sequel::Model
  many_to_one :service

  def after_create
    x = ServiceDataLatest.find(service_id: service_id)
    if x
      x.description = description
      x.status = status
      x.save
    else
      ServiceDataLatest.create(service_id: service_id, description: description, status: status)
    end
  end
end
