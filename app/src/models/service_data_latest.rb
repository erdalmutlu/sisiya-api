# frozen_string_literal: true

# Class for service_data_latest model
class ServiceDataLatest < Sequel::Model
  many_to_one :service
end
