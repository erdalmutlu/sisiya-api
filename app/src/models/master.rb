# frozen_string_literal: true

# Class for master model
class Master < Sequel::Model
  attr_accessor :master_key
end
