# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'users' do |r|
    r.is Integer do |id|
      r.get do
        r.halt(*run_interaction(Interactions::GetUser, path_params: { id: id }))
      end
      # r.put do
      #   r.halt(*run_interaction(Interactions::UpdateUser, path_params: { id: id }))
      # end
      # r.delete do
      #   r.halt(*run_interaction(Interactions::DeleteUser, path_params: { id: id }))
      # end
    end
    r.get do
      r.halt(*run_interaction(Interactions::GetUsers))
    end
    r.post do
      r.halt(*run_interaction(Interactions::CreateUser))
    end
  end
end
