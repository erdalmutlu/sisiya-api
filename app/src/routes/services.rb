# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'services' do |r|
    r.is Integer do |id|
      r.get do
        r.halt(*run_interaction(Interactions::GetService, path_params: { id: id }))
      end
      # r.put do
      #   r.halt(*run_interaction(Interactions::UpdateService, path_params: { id: id }))
      # end
      # r.delete do
      #   r.halt(*run_interaction(Interactions::DeleteService, path_params: { id: id }))
      # end
    end
    r.get do
      r.halt(*run_interaction(Interactions::GetServices))
    end
    # r.post do
    #   r.halt(*run_interaction(Interactions::CreateService))
    # end
  end
end
