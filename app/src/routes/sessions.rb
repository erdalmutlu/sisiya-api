# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'sessions' do |r|
    r.is Integer do |id|
      # r.get do
      #   r.halt(*run_interaction(Interactions::GetSession, path_params: { id: id }))
      # end
      # r.put do
      #   r.halt(*run_interaction(Interactions::UpdateSession, path_params: { id: id }))
      # end
      r.delete do
        r.halt(*run_interaction(Interactions::DeleteSession, path_params: { id: id }))
      end
    end
    # r.get do
    #   r.halt(*run_interaction(Interactions::GetSessions))
    # end
    r.post do
      r.halt(*run_interaction(Interactions::CreateSession))
    end
  end
end
