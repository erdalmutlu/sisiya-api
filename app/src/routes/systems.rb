# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'systems' do |r|
    r.on 'services' do
      r.get do
        r.halt(*run_interaction(Interactions::GetSystemsServices))
      end
    end

    r.is Integer do |id|
      r.get do
        r.halt(*run_interaction(Interactions::GetSystem, path_params: { id: id }))
      end
      # r.put do
      #   r.halt(*run_interaction(Interactions::UpdateSystem, path_params: { id: id }))
      # end
      # r.delete do
      #   r.halt(*run_interaction(Interactions::DeleteSystem, path_params: { id: id }))
      # end
    end
    r.get do
      r.halt(*run_interaction(Interactions::GetSystems))
    end
    # r.post do
    #   r.halt(*run_interaction(Interactions::CreateSystem))
    # end
  end
end
