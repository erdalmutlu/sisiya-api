# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'auth', &:hash_routes

  hash_routes("/#{API_PATH_PREFIX}/auth").on 'token' do |r|
    r.post do
      r.halt(*run_interaction(Interactions::CreateToken))
    end
  end

  hash_routes("/#{API_PATH_PREFIX}/auth").on 'refresh' do |r|
    r.post do
      r.halt(*run_interaction(Interactions::RefreshToken))
    end
  end
end
