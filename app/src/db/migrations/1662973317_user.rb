# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:user) do
      primary_key   :id
      String        :email, null: false
      String        :username, null: false
      String        :account_uuid, null: false
      index         :email, unique: true
    end
  end

  down do
    drop_table(:user)
  end
end
