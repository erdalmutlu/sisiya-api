# frozen_string_literal: true

module Interactions
  # Search system interraction for systems endpoint
  class GetSystems < ::Interactions::BaseAuthenticated
    # even if the input (query, header) hash has more keys active interaction
    # removes all keys that not defined as nested filter

    hash :query_params do
      string :name, default: nil # nested filter
    end

    # to see all headers (not stripped)
    # hash :headers, strip: false

    def execute
      x = System.dataset
      x = x.where(Sequel.lit('name LIKE ?', "%#{@query_params[:name]}%")) if @query_params[:name]
      x = x.limit(@query_params[:qlimit]) if @query_params[:qlimit]
      return api_response http: :no_content if x.empty?

      api_response http: :http_ok, result: get_result(x)
    rescue StandardError => e
      api_response http: :internal_server_error, message: 'Something went wrong!', error: e
    end

    def get_result(data)
      a = []
      data.each do |i|
        h = i.values
        h[:status_image] = i.status_image
        h[:status_description] = i.status_description
        a.push(h)
      end
      a
    end
  end
end
