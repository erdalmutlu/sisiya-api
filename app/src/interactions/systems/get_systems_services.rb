# frozen_string_literal: true

module Interactions
  # Search system interraction for systems endpoint
  class GetSystemsServices < ::Interactions::BaseAuthenticated
    # even if the input (query, header) hash has more keys active interaction
    # removes all keys that not defined as nested filter

    hash :query_params do
      string :name, default: nil # nested filter
    end

    # to see all headers (not stripped)
    # hash :headers, strip: false

    def execute # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      ds = SystemService.dataset.join(:status, id: :status_id)
      ds = ds.join(:service, id: Sequel[:system_service][:service_id])
      ds = ds.join(:system, id: Sequel[:system_service][:system_id])
      ds = ds.where(Sequel.lit('system.name LIKE ?', "%#{@query_params[:name]}%")) if @query_params[:name]
      ds = ds.limit(@query_params[:qlimit]) if @query_params[:qlimit]
      ds = ds.select_all(:system_service)
      ds = ds.select_append(Sequel.as(Sequel[:status][:name], :status_name))
      ds = ds.select_append(Sequel.as(Sequel[:service][:name], :service_name))
      ds = ds.select_append(Sequel.as(Sequel[:system][:name], :system_name))
      return api_response http: :no_content if ds.empty?

      api_response http: :http_ok, result: ds.map(&:values)
    rescue StandardError => e
      api_response http: :internal_server_error, message: 'Something went wrong!', error: e
    end
  end
end
