# frozen_string_literal: true

module Interactions
  # Create system interraction for systems endpoint
  class GetSystem < ::Interactions::BaseAuthenticated
    hash :path_params do
      integer :id
    end

    def execute
      x = System.find(id: path_params[:id])
      return api_response http: :ok, result: x.values if x

      api_response http: :no_content
    rescue StandardError => e
      api_response http: :internal_server_error,
                   message: 'Something went wrong with finding the system!', error: e
    end
  end
end
