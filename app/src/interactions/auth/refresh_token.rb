# frozen_string_literal: true

module Interactions
  # Refresh token interraction for auth/refresh endpoint
  class RefreshToken < ::Interactions::BaseNotAuthenticated
    include ::Helpers::Authentication

    hash :headers do
      string :authorization, default: nil
    end

    def execute # rubocop:disable Metrics/AbcSize
      payload = authorize_token(headers[:authorization])
      return api_response http: :unauthorized, message: 'Refresh token is not valid!' unless payload

      if Time.now.to_i > payload.first['exp']
        return api_response http: :unauthorized, message: 'Refresh token is expired!'
      end

      user = User.find(id: payload.first['user_id'])
      return api_response http: :unauthorized, message: 'User not found!' unless user

      api_response http: :ok, result: { access_token: generate_token(user, Settings.access_token_lifetime),
                                        refresh_token: generate_token(user, Settings.refresh_token_lifetime) }
    end
  end
end
