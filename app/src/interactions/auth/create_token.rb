# frozen_string_literal: true

module Interactions
  # Create token interraction for auth/token endpoint
  class CreateToken < ::Interactions::BaseNotAuthenticated
    include ::Helpers::Authentication

    hash :body_params do
      string  :email
      string  :password
    end

    def execute
      APP_LOGGER.debug "interactions/auth/create_token.rb:execute: body_params: #{body_params}"
      user = User.find(email: body_params[:email])
      unless user&.password_valid?(body_params[:password])
        return api_response http: :unauthorized, message: 'Email or password not valid!'
      end

      api_response http: :ok, result: { access_token: generate_token(user, Settings.access_token_lifetime),
                                        refresh_token: generate_token(user, Settings.refresh_token_lifetime) }
    end
  end
end
