# frozen_string_literal: true

require HELPERS_PATH.join('application.rb')
module Interactions
  # Base
  class Base < ActiveInteraction::Base
    include ::Helpers::Application

    object :options, class: Hash
  end
end
