# frozen_string_literal: true

module Interactions
  # Get users interraction for users endpoint
  class GetUsers < ::Interactions::BaseAuthenticated
    hash :query_params do
      string :email, default: nil
      string :username, default: nil
    end

    def execute # rubocop:disable Metrics/AbcSize
      x  = User.dataset
      x  = x.where(Sequel.lit('email LIKE ?', "%#{@query_params[:email]}%")) if @query_params[:email]
      x  = x.where(Sequel.lit('username LIKE ?', "%#{@query_params[:username]}%")) if @query_params[:username]
      x  = x.limit(@query_params[:qlimit]) if @query_params[:qlimit]
      return api_response http: :no_content if x.empty?

      api_response http: :http_ok, result: x.map(&:values)
    rescue StandardError => e
      api_response http: :internal_server_error, message: 'Something went wrong!', error: e
    end
  end
end
