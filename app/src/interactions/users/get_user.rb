# frozen_string_literal: true

module Interactions
  # Create session interraction for sessions endpoint
  class GetUser < ::Interactions::BaseAuthenticated
    def execute
      APP_LOGGER.debug 'interactions/users/get_user.rb:execute: Getting a user...'
      api_response http: :ok, result: helper.session['xyz']
    end
  end
end
