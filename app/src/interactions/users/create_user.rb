# frozen_string_literal: true

module Interactions
  # Create session interraction for sessions endpoint
  class CreateUser < ::Interactions::BaseAuthenticated
    hash :body_params do
      string  :email
      string  :username
      string  :password
    end

    def execute
      user = User.create(email: body_params[:email],
                         username: body_params[:username],
                         password: body_params[:password])
      return api_response http: :bad_request, message: 'User not created' unless user

      api_response http: :bad_request, result: user.values
    end
  end
end
