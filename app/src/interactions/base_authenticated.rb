# frozen_string_literal: true

require INITIALIZERS_PATH.join('db.rb')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')
require HELPERS_PATH.join('authentication.rb')

module Interactions
  # Base class for authenticated interactions
  # class BaseAuthenticated < ActiveInteraction::Base
  class BaseAuthenticated < Interactions::Base
    # include ::Helpers::Application
    include ::Helpers::Authentication

    # object :options, class: Hash

    hash :headers do
      string :authorization, default: nil
    end

    set_callback :validate, :after, lambda {
      # APP_LOGGER.debug "interactions/base_authenticated.rb:set_callback: #{headers}"
      # errors.add(:api_key, 'not valid!', http_code: :unauthorized) unless true)
      errors.add(:access_token, 'not valid!', http_code: :unauthorized) unless authorize_token(headers[:authorization])
    }
  end
end
