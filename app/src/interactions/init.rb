# frozen_string_literal: true

require HELPERS_PATH.join('application.rb')

%w[
  base
  base_authenticated
  base_not_authenticated
  auth/create_token
  auth/refresh_token
  users/get_user
  users/get_users
  services/get_service
  services/get_services
  systems/get_system
  systems/get_systems
  systems/get_systems_services
  interaction_runner
].each do |s|
  require INTERACTIONS_PATH.join s
rescue StandardError
  APP_LOGGER.fatal "interactions/init.rb: Can not load interaction #{s}!"
end
