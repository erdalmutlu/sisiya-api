# frozen_string_literal: true

require INITIALIZERS_PATH.join('db.rb')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')

module Interactions
  # Base class for not authenticated interactions
  # class BaseAuthenticated < ActiveInteraction::Base
  class BaseNotAuthenticated < Interactions::Base
    # include ::Helpers::Application

    # object :options, class: Hash

    # def helper
    #   options[:sequel_account_helper]
    # end
  end
end
