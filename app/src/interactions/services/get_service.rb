# frozen_string_literal: true

module Interactions
  # Create session interraction for sessions endpoint
  class GetService < ::Interactions::BaseAuthenticated
    hash :path_params do
      integer :id
    end

    def execute
      x = Service.find(id: path_params[:id])
      return api_response http: :http_ok, result: x.values if x

      api_response http: :no_content
    rescue StandardError => e
      api_response http: :internal_server_error,
                   message: 'Something went wrong with finding the service!', error: e
    end
  end
end
