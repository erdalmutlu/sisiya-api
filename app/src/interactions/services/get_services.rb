# frozen_string_literal: true

module Interactions
  # Search services interraction for systems endpoint
  class GetServices < ::Interactions::BaseAuthenticated
    hash :query_params do
      string :name, default: nil
    end

    def execute
      x  = Service.dataset
      x  = x.where(Sequel.lit('name LIKE ?', "%#{@query_params[:name]}%")) if @query_params[:name]
      x  = x.limit(@query_params[:qlimit]) if @query_params[:qlimit]
      return api_response http: :no_content if x.empty?

      api_response http: :http_ok, result: x.map(&:values)
    rescue StandardError => e
      api_response http: :internal_server_error, message: 'Something went wrong!', error: e
    end
  end
end
