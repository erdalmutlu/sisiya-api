# frozen_string_literal: true

module SisiyaServiceStatus
  # service status mappings
  module ClassMethods
    def status_values # rubocop:disable Metrics/MethodLength
      # {
      #   "info": 1,        # 2^0
      #   "ok": 2,          # 2^1
      #   "warning": 4,     # 2^2
      #   "error": 8,       # 2^3
      #   "noreport": 16,   # 2^4
      #   "unavailable": 32 # 2^5
      # }
      # {
      #   "info": 0,
      #   "ok": 1,
      #   "warning": 2,
      #   "error": 3,
      #   "noreport": 4,
      #   "unavailable": 5
      # }
      {
        info: 1, #          2^0
        ok: 2, #            2^1
        warning: 4, #       2^2
        error: 8, #         2^3
        noreport: 16, #     2^4
        unavailable: 32, #  2^5
        mwarning: 64, #     2^6 maintanance & warning
        merror: 128, #      2^7 maintanence & error
        mnoreport: 256, #   2^8 maintenance & noreport
        munavailable: 512 # 2^9 maintanance & unavailable
      }
    end
  end
end
