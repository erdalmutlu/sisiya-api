# frozen_string_literal: true

module Helpers
  # Authentication
  module Authentication
    def authorize_token(token)
      # APP_LOGGER.debug "helpers/authentication.rb/current_user: token: #{token}"
      return nil unless token

      begin
        token = token.gsub('Bearer', '').strip
        JWT.decode token, Settings.jwt_secret, true, { algorithm: Settings.jwt_algorithm }
      rescue StandardError => e
        APP_LOGGER.error e.backtrace
        nil
      end
    end

    def current_user(token)
      # APP_LOGGER.debug "helpers/authentication.rb:current_user: token: #{token}"
      payload = authorize_token(token)
      return unless payload

      APP_LOGGER.debug "helpers/authentication.rb:current_user: payload: #{payload}"
      User.find(id: payload.first['user_id'])
    end

    def generate_token(user, lifetime)
      # check standard for payload names: https://www.iana.org/assignments/jwt/jwt.xhtml
      payload = { user_id: user.id, username: user.username, email: user.email,
                  account_uuid: user.account_uuid,
                  exp: (Time.now + lifetime).to_i,
                  iat: Time.now.to_i }
      JWT.encode payload, Settings.jwt_secret, Settings.jwt_algorithm
    end
  end
end
