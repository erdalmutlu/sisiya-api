# frozen_string_literal: true

# ActiveInteractionRunner
class ActiveInteractionRunner
  def self.execute(interaction, request, options = {}) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    # generate hash while downcasing keys
    headers = request.env.each_with_object({}) do |(k, v), acc|
      if k =~ /^http_(.*)/i
        acc[::Regexp.last_match(1).downcase] =
          v
      end
    end
    body = request.body.read
    request_params = { ip: request.ip }
    body_params = body.empty? ? nil : JSON.parse(body).symbolize_keys
    APP_LOGGER.debug "helpers/active_interaction_runner.rb:execute: #{interaction.name}"
    APP_LOGGER.debug "helpers/active_interaction_runner.rb:execute: headers: #{headers}"
    APP_LOGGER.debug "helpers/active_interaction_runner.rb:execute: query params: #{request.params}"
    APP_LOGGER.debug "helpers/active_interaction_runner.rb:execute: path params: #{options[:path_params]}"

    out = interaction.run(path_params: options[:path_params],
                          query_params: request.params,
                          body_params: body_params,
                          headers: headers,
                          request_params: request_params)
    if out.valid?
      APP_LOGGER.debug 'helpers/active_interaction_runner.rb: out is valid'
      APP_LOGGER.debug "helpers/active_interaction_runner.rb: out.result: #{out.result}"
      out.result
    else
      APP_LOGGER.debug 'helpers/active_interaction_runner.rb: out is not valid => BAD REQUEST'
      APP_LOGGER.debug out.errors
      http_code = out.errors.first.options[:http] || :bad_request
      [HTTP[http_code], { message: out.errors.messages }.to_json]
    end
  rescue StandardError => e
    APP_LOGGER.debug 'helpers/active_interaction_runner.rb: SOMETHING UNKNOWN IS HAPPENING'
    APP_LOGGER.error "helpers/active_interaction_runner.rb: #{e.class}"
    APP_LOGGER.error "helpers/active_interaction_runner.rb: #{e.backtrace.join("\n\t ")}"
    [400, { message: 'unhandled error' }.to_json]
  end
end
