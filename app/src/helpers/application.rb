# frozen_string_literal: true

module Helpers
  # Application
  module Application
    # http: the HTTP return code
    # result: any results e.g. client
    # message: a message describing the action, usually this is an error message
    # error: is the error object, which will be added to the message
    def api_response(http:, result: nil, message: nil, error: nil)
      # if error
      #   APP_LOGGER.error "helpers/application.rb:a_response: #{error.class}: #{error.message}\n"
      #   APP_LOGGER.error "helpers/application.rb:a_response: #{error.backtrace.join("\n\t ")}"
      # end

      out = {}
      out = result if result
      out[:message] = message if message
      out[:message] += " #{error.message}".gsub("\n", ' ') if error

      if out.empty? && http != :ok
        [HTTP[http]]
      else
        [HTTP[http], { 'content-type' => 'application/json' }, out.to_json]
      end
    end

    def map_sequel_errors(sequel_errors, active_interaction_errors)
      sequel_errors.each_key do |k|
        sequel_errors[k].each do |e|
          active_interaction_errors.add(k, e)
        end
      end
    end

    def try_exec
      yield
    rescue StandardError => e
      errors.add(:error, 'An error occured!')
      APP_LOGGER.error "helpers/application.rb:try_exec: #{e.message}\n"
      APP_LOGGER.error "helpers/application.rb:try_exec: #{e.backtrace.join("\n\t ")}"
    end
  end
end
